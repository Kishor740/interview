package longestCommonPrefix;

import java.util.Arrays;

public class LongestCommonPrefix {
	
	public static String longestCommonPrefix(String[] strs) {
        int arrLen = strs.length;
        StringBuilder lcp = new StringBuilder();
        if(arrLen == 0){
           lcp.append("");
        }else if(arrLen == 1){
            lcp.append(strs[0]);
        }else{
            Arrays.sort(strs);
            int minLen = Math.min(strs[0].length(), strs[arrLen-1].length());
            for(int i=0; i<minLen; i++){
                if(strs[0].charAt(i) == strs[arrLen-1].charAt(i)){
                    lcp.append(strs[0].charAt(i));
                }else{
                    return lcp.toString();
                }
            }
        }
        return lcp.toString();
    }

	public static void main(String[] args) {
		String[] str = {"flower","flow","flight"};
		String result = longestCommonPrefix(str);
		System.out.println(result);
	}

}
