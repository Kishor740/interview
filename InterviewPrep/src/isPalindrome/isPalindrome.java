package isPalindrome;

public class isPalindrome {
	
	public static boolean isPal(int x) {
		boolean isPal = false;
		int result = 0;
		int check = x;
		int rem;
		while(x>0) {
			rem = x%10;
			result = result*10+rem;
			x=x/10;
		}
//		System.out.println(result);
//		System.out.println(check);
		if(result == check) {
			isPal = true;
		}
		
		return isPal;
	}

	public static void main(String[] args) {
		System.out.println(isPal(-101));
		

	}

}
