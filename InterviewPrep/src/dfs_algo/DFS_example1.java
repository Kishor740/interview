package dfs_algo;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DFS_example1 {
	
	public static int[] row = {-1,-1,-1,0,0,1,1,1};
	public static int[] column = {-1,0,1,-1,1,-1,0,1}; 
	
	public static boolean isSafe(int x, int y, boolean[][] processed) {
		return (x>=0 && x<processed.length) &&(y>=0 && y<processed[0].length) && !processed[x][y];
	}
 	
	public static void searchBoggle(char[][] board, Set<String> words, int i, int j, 
			Set<String> result, boolean[][] processed, String path) {
		
		//mark current node as processed
		processed[i][j] = true;
		
		//update the current path with current character 
		path += board[i][j];
		
		//check whether the path is present in the set
		if(words.contains(path)) {
			result.add(path);
		}
		
		//check all 8 possible movement from the current cell
		for(int k=0; k<row.length; k++) {
			//check to see if its valid to move to the cell or is it already processed
			if(isSafe(i+row[k], j+column[k], processed)) {
				searchBoggle(board, words, i+row[k], j+column[k], result, processed, path);
			}
		}
		processed[i][j] = false;
	}
	
	
	public static Set<String> searchBoggle(char[][]board, Set<String> words){
		Set<String> result = new HashSet<String>();
		//base case
		if(board.length == 0) return result;
		
		//M X N board
		int M = board.length;
		int N = board[0].length;
		
		//construct a matrix to store whether a cell is process or not
		boolean[][] processed = new boolean[M][N];
		
		//generate all possible words
		for(int i=0; i<M; i++) {
			for(int j=0; j<N; j++) {
				searchBoggle(board, words, i, j, result, processed,"");
			}
		}
		
		
		
		return result;
	}

	public static void main(String[] args) {
		// Find all valid words from the given set of words
		char[][] board =
	        {
	            {'M', 'S', 'E', 'F'},
	            {'R', 'A', 'T', 'D'},
	            {'L', 'O', 'N', 'E'},
	            {'K', 'A', 'F', 'B'}
	        };
		
		Set<String> words = Stream.of("START","NOTE","SAND","STONED")
				.collect(Collectors.toSet());
		Set<String> result = searchBoggle(board, words);
		System.out.println(result);

	}

}
