package twoSum;

import java.util.HashMap;
import java.util.Map;

public class twoSum {
	
	 public static int[] twoSums(int[] nums, int target) {
	     Map<Integer, Integer> mapval = new HashMap<Integer, Integer>();  
		 int[] result = new int[2];
	       for(int i=0; i<nums.length; i++) {
	    	  if(mapval.get(target-nums[i]) !=null) {
	    		  result[0] = mapval.get(target-nums[i]);
	    		  result[1] = i;
	    	  }
	    	   
	    	   mapval.put(nums[i], i);
	       }
	       
	       return result;
	    }

	public static void main(String[] args) {
		int[] nums = {2,15,11,7,6,0};
		int target = 9;
		//find the array index which gives the target as sum
		int[] result = twoSums(nums, target);
		System.out.println("["+result[0]+","+result[1]+"]");

	}

}
