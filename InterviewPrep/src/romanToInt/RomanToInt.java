package romanToInt;

public class RomanToInt {

	public static int romanToInt(String s) {
		char[] stchar = s.toCharArray();
		int result = 0;
		for(int i=0; i<s.length(); i++) {
			char currChar = stchar[i];
			if(stchar[i] == 'I') {
				result += 1;
			}else if(stchar[i] == 'V') {
				result += 5;
			}else if(stchar[i] == 'X') {
				result += 10;
			}else if(stchar[i] == 'L') {
				result += 50;
			}else if(stchar[i] == 'C') {
				result += 100;
			}else if(stchar[i] == 'D') {
				result += 500;
			}else if(stchar[i] == 'M') {
				result += 1000;
			}
			if(i>=1) {
				if((currChar == 'V' || currChar == 'X') && stchar[i-1] == 'I') {
					result-=2;
				}else if((currChar == 'L' || currChar == 'C') && stchar[i-1] == 'X') {
					result-=20;
				}else if((currChar == 'D' || currChar == 'M') && stchar[i-1] == 'C') {
					result-=200;
				}
			}
		}
		return result;
	}
	public static void main(String[] args) {
		
//		Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
//
//		Symbol       Value
//		I             1
//		V             5
//		X             10
//		L             50
//		C             100
//		D             500
//		M             1000
//		There are six instances where subtraction is used:
//
//			I can be placed before V (5) and X (10) to make 4 and 9. 
//			X can be placed before L (50) and C (100) to make 40 and 90. 
//			C can be placed before D (500) and M (1000) to make 400 and 900.
//		
//		romanToInt("IV");
		System.out.print(romanToInt("DCXXI"));
	}

}
